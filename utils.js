
const _ = require('busyman');
const lwm2mId = require('lwm2m-id');
var SmartObject = require('@lwmqn/smartobject');

class Utils {

  constructor(defs) {
    this.defs = defs;
  }

  /**
   * Breaks a object path up into it's components.
   * @param path:string
   * @return [oid, iid, rid, idx]
   */
  static getPathComponents(path) {
    var [obj, rsc] = path.split('.');
    obj = obj.replace('[', '/').replace(']','');
    rsc = rsc.replace('[', '/').replace(']','');
    path = obj + '/' + rsc;
    return path.split('/');
  }

  /**
   * Convert object to SmartObject
   * @param obj:object
   */
  static objToSmartObject(obj) {
    var so = new SmartObject();
    _.forEach(obj, (o, oid) => {
      _.forEach(o, (inst, iid) => {
        so.init(oid, iid, inst);
      });
    });
    return so;
  }

  /**
   * Maps smart object to flat JSON
   * @param {smart object} obj
   * @param {mapping object} map
   */
  static getMapValues(obj, map) {
    var result = {};
    _.forEach(map, (val, key) => {
      if(typeof val === 'object') {
        if (Array.isArray(val['attr'])) {
          var vals = [];
          _.forEach(val['attr'],(path) => {
            if(_.has(obj, path)) {
              vals.push(_.get(obj, path));
            }
          });
          if(vals.length > 0)
            result[key] = vals.join(',');
          // const v: string[] = val['attr'];
          // value = v.join(',');
        } else if(_.has(obj, val['attr']))
          result[key] = _.get(obj, val['attr']);
      }
      else if(_.has(obj, val)) {
        result[key] = _.get(obj, val);
      }
    });
    return result;
  }

  /**
   * Convert Key:Value JSON to SO
   * @param {*} kv
   * @param {*} map
   */
  static mapKeyValuesToSmartObject(kv, map) {
    var so = {};
    _.forEach(kv, (val) => {
      if(_.has(map,val.key)) {
        _.set(so,map[val.key],val.value)
      }
    })
    return so;
  }

  /**
   * Convert SO to Key:Value JSON based on Map
   * @param {*} kv 
   * @param {*} map 
   */
  static mapSmartObjectToKeyValues(so, map) {
    var kv = [];
    _.forEach(map, (path, key) => {
      if(_.has(so,path)) {
        kv.push({key, value: _.get(so, path)})
      }
    })
    return kv;
  }

  static mapJsonToSmartObject(json, map) {
    var so = {};
    _.forEach(json, (val, key) => {
      if(_.has(map,key)) {
        _.set(so,map[key],val)
      }
    })
    return so;
  }

  convertJsonToSimpleSO(json, o, mapping) {
    let obj = _.clone(o);
    _.forEach(json, (v, key) => {
      if (key in mapping.payload) {
        let paths = mapping.payload[key];
        if (paths === undefined) {
          console.log(key);
        }
        if (typeof paths === 'string') paths = [paths];
        paths.forEach((path) => {
          const [oid, iid, rid, idx] = Utils.getPathComponents(path);
          const val = this.convertToDataType(oid, rid, v);

          // create object
          if (! (oid in obj)) {
            obj[oid] = {};
          }

          // create instance
          if (! (iid in obj[oid])) {
            obj[oid][iid] = {};
          }

          // get instance
          const inst = obj[oid][iid];

          // if an index giving for multiple values
          if (idx !== undefined) {
            if (! (rid in inst)) {
              inst[rid] = [];
            }

            const rsrc = this.getResourceDefinition(oid, rid);

            // init missing values if idx larger than current array size
            const array = inst[rid];
            for (let i = array.length; i <= idx; i++) {
              array.push(rsrc.init);
            }

            // add value to corrent position in array
            array[idx] = val;
          } else {
            inst[rid] = val;
          }
        });
      }
    });
    return obj;
  }

  // creates a instance of the SmartObject using the resource definition
  getDefaultObjectResources() {
    var obj = {};
    _.forEach(this.defs.definition, (o, oid) => {
      var inst = {};
      obj[oid] = [inst];
      _.forEach(o.resources, (rsrc, rid) => {
        inst[rid] = rsrc;
      });
    });
    return obj;
  }

  getOidNum(oid) {
    // lwm2m-id itself will throw TypeError if oid is not a string and not a number
    const oidItem = lwm2mId.getOid(oid)
    return oidItem ? oidItem.value : oid
  }

  getRidNumber(oid, rid) {
    // lwm2m-id itself will throw TypeError if rid is not a string and not a number
    const ridItem = lwm2mId.getRid(oid, rid)
    return ridItem ? ridItem.value : rid
  }

  getSixthSenseRidKey(rsrcs, key) {
    var rsrc = rsrcs[key];
    return rsrc !== undefined ? rsrc.rid : key;
  }


  /** ********************************************************************* */
  /** * Mitrack definition                                               ** */
  /** ********************************************************************* */


  getObjectDefinition(oid) {
    try {
      return this.defs.definition[oid];
    } catch (error) {
      return undefined;
    }
  }

  getResourceDefinition(oid, rid) {
    try {
      return this.getObjectDefinition(oid).resources[rid];
    } catch (error) {
      return undefined;
    }
  }

  convertToDataType(oid, rid, val) {
    try {
      const rsrc = this.getResourceDefinition(oid, rid);
      switch(rsrc.type) {
        case 'integer': return parseInt(val);
        case 'boolean': return 'true' === val;
        case 'float': return parseFloat(val);
        case 'time': return parseInt(val);
        default: return val;
      }
    } catch (error) {
      return undefined;
    }
  }

  /** ********************************************************************* */
  /** * Data Dumper                                                      ** */
  /** ********************************************************************* */


  dump(so, useNumbers) {
    const dumped = {}
    _.forEach(so, (o, oidKey) => {
      var obj, key;

      // check object belongs to L2M2M defined objects
      const oidItem = lwm2mId.getOid(oidKey)

      // if not found then look in definition
      if (!oidItem) {
        obj = this.defs.definition[oidKey];
        if (obj !== undefined) {
          key = useNumbers ? obj.oid : oidKey;
          obj = this.dumpSixthSenseObject(o, obj.resources, useNumbers);
        }
      }
      if (obj === undefined)
      {
        key = useNumbers ? this.getOidNum(oidKey) : oidKey;
        obj = this.dumpObject(o, useNumbers);
      }
      dumped[key] = obj;
    })
    return dumped
  }


  /** ********************************************************************* */
  /** * Data Dumper for SixthSense Proprietory Objects                   ** */
  /** ********************************************************************* */

  dumpSixthSenseObject(o, rsrcs, useNumbers) {
    const dumped = {}

    _.forEach(o, (inst, instId) => {
      dumped[instId] = this.dumpSixthSenseInstance(inst, rsrcs, useNumbers)
    })

    return dumped
  }

  dumpSixthSenseInstance(inst, rsrcs, useNumbers) {
    const dumped = {}

    _.forEach(inst, (rval, ridKey) => {
      let clonedObj

      // do not dump keys: 'oid', 'iid'
      if (ridKey === 'oid' || ridKey === 'iid' || _.isFunction(rval)) 
        return

      var key = useNumbers ? this.getSixthSenseRidKey(rsrcs, ridKey) : ridKey;

      if (_.isObject(rval)) {
        clonedObj = this.cloneResourceObject(rval)
        dumped[key] = clonedObj
      } else if (!_.isFunction(rval)) {
        dumped[key] = rval
      }
    })
    return dumped
  }

  /** ********************************************************************************** */
  /** * Data Dumper for Standard LWM2M Objects                                        ** */
  /** ********************************************************************************** */

  dumpObject(obj, useNumbers) {
    const dumped = {}

    _.forEach(obj, (inst, instId) => {
      dumped[instId] = this.dumpInstance(inst, useNumbers)
    })

    return dumped
  }

  dumpInstance(inst, useNumbers) {
    const dumped = {}
    const oid = inst.oid;

    _.forEach(inst, (rval, ridKey) => {
      let clonedObj

      // do not dump keys: 'oid', 'iid'
      if (ridKey === 'oid' || ridKey === 'iid' || _.isFunction(rval)) return

      var key = useNumbers ? this.getRidNumber(oid, ridKey) : ridKey;

      if (_.isObject(rval)) {
        clonedObj = this.cloneResourceObject(rval)
        dumped[key] = clonedObj
      } else if (!_.isFunction(rval)) {
        dumped[key] = rval
      }
    })
    return dumped
  }


  cloneResourceObject(rObj) {
    const cloned = {}

    if (rObj._isCb) {
      _.forEach(rObj, (rval, rkey) => {
        if (rkey === 'read' || rkey === 'write' || rkey === 'write') cloned[rkey] = `_${rkey}_` // '_read_', '_write_', '_exec_'
      })
    } else {
      _.forEach(rObj, (rval, rkey) => {
        if (_.isFunction(rval)) return

        cloned[rkey] = rval
      })
    }

    delete cloned._isCb
    return cloned
  }

}

module.exports = Utils
